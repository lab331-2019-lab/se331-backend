package se331.backend.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.backend.rest.entity.Student;

import java.util.List;


public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findAll();
    Student findById(long id);
    Student save(Student student);
}
