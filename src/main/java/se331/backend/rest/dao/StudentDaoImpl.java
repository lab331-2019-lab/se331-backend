package se331.backend.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.backend.rest.entity.Student;
import se331.backend.rest.repository.StudentRepository;

import java.util.List;

@Slf4j
@Profile("Dao")
@Repository
public class StudentDaoImpl implements StudentDao {

    @Autowired
    StudentRepository studentRepository;
    @Override
    public List<Student> getAllStudents() {
        log.info("Find all students from database.");
        return studentRepository.findAll();
    }

    @Override
    public Student findById(long id) {
        log.info("Find student with id {} from database.", id);
        return studentRepository.findById(id);
    }

    @Override
    public Student saveStudent(Student student) {
        log.info("Save student with id {} to database.", student);
        return studentRepository.save(student);
    }
}
