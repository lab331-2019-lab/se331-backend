package se331.backend.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.backend.rest.entity.Teacher;
import se331.backend.rest.repository.TeacherRepository;

import java.util.List;

@Slf4j
@Profile("Dao")
@Repository
public class TeacherDaoImpl implements TeacherDao{
    @Autowired
    TeacherRepository teacherRepository;
    @Override
    public List<Teacher> getAllTeachers() {
        log.info("Find all teachers from database.");
        return teacherRepository.findAll();
    }

    @Override
    public Teacher findById(long id) {
        log.info("Find teacher with id {} from database.", id);
        return teacherRepository.findById(id);
    }

    @Override
    public Teacher saveTeacher(Teacher teacher) {
        log.info("Save teacher with id {} to database.", teacher);
        return teacherRepository.save(teacher);
    }
}
